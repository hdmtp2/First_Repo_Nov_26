#!/usr/bin/env python3

"""
#+AUTHOR      : https://github.com/hDmtP

#+DESCRIPTION : RSS Feed fetcher. I have hardcoded the xmls that Im keen of in this file. 
                It sends request once a day via a cronjob. If new feed is detected it 
                updates the current no of blogs for that link in feed.db.
                For adding new link I have to insert them manually in the db.
                feed.db need to be in the same dir with this __file__

#+TODO        : Add function for Inserting new urls
"""
import os.path
import feedparser as fp
import sqlite3 as sq
import asyncio

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DB_PATH = os.path.join(BASE_DIR, 'feed.db')
con = sq.connect(DB_PATH)
cur = con.cursor()

def update_row_in_db(url: str, no=0):
    cur.execute("UPDATE my_feeds SET blog_no =? WHERE url=?", (no, url))
    con.commit()

feed_dict = {
    "url" : [
            "https://lukesmith.xyz/index.xml", 
            "https://bugswriter.com/blog/atom.xml",
            "https://hdmtp.github.io/feed.xml",
            "https://gnulinuxindia.org/index.xml",
            "https://gnulinuxindia.org/fossevents.xml",
            "https://neovoid.tk/feed.xml",
            "https://saurabh.machave.in/feed.xml",
            "https://krolyxon.tildevarsh.in/atom.xml",
            "https://hitarththummar.xyz/atom.xml"
            ],
    "name" : [
            "LukeSmith.txt",
            "Bugswriter.txt",
            "hdmtp.txt",
            "gnulinuxindia.txt",
            "gnulinuxindia_fossevents.txt",
            "neovoid.txt",
            "saurav.machave.txt",
            "krolyxon.txt",
            "hitarththummar.txt"
            ]
}

async def fetch_feed(url):
    try:
        cur.execute("SELECT blog_no FROM my_feeds WHERE url=?", (url,))
        no = cur.fetchall()[0][0]
        d = fp.parse(url)
        if (len(d.entries) > no):
            no = len(d.entries)
            update_row_in_db(url, no)
            e = d.entries[0]
            print(url)
            print(e.title)
            print(e.published)
            print(e.links[0].href)
            print("\n\n=============================\n\n")
        # print(e)
        # print(e.summary)
    except Exception as e:
        print(e.args)
    except KeyboardInterrupt:
        print("KeyboardInterrupt!") 
    finally:
        print(f"\n{url} fetched successfully")

async def display_feed():
    await asyncio.gather(*[fetch_feed(feed_dict["url"][i]) for i in range(len(feed_dict["url"]))])

if __name__ == "__main__":

    asyncio.run(display_feed())
    con.close()
